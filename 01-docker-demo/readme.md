# DOCKER DEMO

In this demo you will learn more about the basic docker, how to use it and how to master it.

## RUNDOWN

Build image using this script `docker build -t php-app.`

Run the docker image (automatic create a container instance) `docker run -p 8080:80 --rm php-app`.
This script will automatically remove the container instance after we kill the process. 
You can validate this using this script `docker ps -a`

If you want to keep the container instance after we run the docker images please use
`docker run -p 8080:80 --name=awesome-thing php-app`. You can spin the instance using
`docker start awesome-thing` and you are ready to go to build amazing thing using your docker.

Of course for development this method is not recomended to package our apps. You know, you will 
need to build it first before see the changes. Thanks for docker command we can mount the local 
folder inside the instance. Ok lets go delete the awesome-thing container instance

`$ docker rm awesome-thing`

```
$ docker run -p 8080:80 -v $PWD/src:/var/www/html --name=awesome-thing php-app
$ docker start awesome-thing
$ docker stop awesome-thing
```

## TIPS

Instead of install on separate line like this

```
FROM xxxx
RUN apt-get install x-dev
RUN apt-get install y-dev
```

It's recommended to you to :

1. Chaining your install method
2. Clean your apt-cache in the one line 
3. Add ```--no-install-recommends``` add your apt install 
4. Inspect your docker build image using ```docker history your-image-build``` to see if we can improve the build size
5. Use docker minification tools like https://www.fromlatest.io/#/ to reduce your Dockerfile script

```
FROM xxxx
RUN apt-get install x-dev install y-dev --no-install-recommends && rm  -rf /var/lib/apt/lists/*
```

## PUSH TO DOCKER IMAGE REGISTRY

After you create your image you can share your image to docker registry. 

### Using Docker Hub

If you want to host your image build in https://hub.docker.com/

```
$ docker login
$ docker push <your-image-name>
$ docker pull <your-image-name>
```

#### Using GitLab Registry

If you want to host in to GitLab

```
$ docker login registry.gitlab.com
$ docker push registry.gitlab.com/<username>/<repo>:latest
$ docker push registry.gitlab.com/<username>/<repo>:<tag>
```

## LEARN MORE

You can level up your docker skill in https://www.katacoda.com/courses/docker